//
//  WUTDeviceDetailsViewControllerViewController.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WUTViewController.h"

@class Device;

@interface WUTDeviceDetailsViewController : WUTViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;

- (id)initWithDevice:(Device *)device;

@end
