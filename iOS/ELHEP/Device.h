//
//  Device.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe;

@interface Device : NSManagedObject

@property (nonatomic, retain) NSNumber * deviceID;
@property (nonatomic, retain) NSNumber * verID;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSDate * dateAdded;
@property (nonatomic, retain) NSSet *uniqueProbes;
@end

@interface Device (CoreDataGeneratedAccessors)

- (void)addUniqueProbesObject:(Probe *)value;
- (void)removeUniqueProbesObject:(Probe *)value;
- (void)addUniqueProbes:(NSSet *)values;
- (void)removeUniqueProbes:(NSSet *)values;

@end
