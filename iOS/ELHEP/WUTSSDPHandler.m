//
//  WUTSSDPHandler.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 6/19/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTSSDPHandler.h"
#import "GCDAsyncUdpSocket.h"

#define SSDP_PORT_NUMBER 1900

@implementation WUTSSDPHandler

- (id)init{
    self = [super init];
    if(!self)return nil;
    
    dispatch_queue_t ssdp_queue = dispatch_queue_create("com.mapeddELHEP.SSDPQueue", NULL);
    dispatch_queue_t socket_queue = dispatch_queue_create("com.mapeddELHEP.SocketQueue", NULL);
    
    udpSocket_ = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                               delegateQueue:ssdp_queue
                                                 socketQueue:socket_queue];
    
    NSError *error = nil;
	
	if (![udpSocket_ bindToPort:SSDP_PORT_NUMBER error:&error])
	{
		NSLog(@"Error binding: %@", error);
		return nil;
	}
	if (![udpSocket_ beginReceiving:&error])
	{
		NSLog(@"Error receiving: %@", error);
		return nil;
	}
    
    return self;
}

- (void)startSendingAdvertisements{
    
}

#pragma mark - GCDAsyncUdpSocketDelegate

/**
 * By design, UDP is a connectionless protocol, and connecting is not needed.
 * However, you may optionally choose to connect to a particular host for reasons
 * outlined in the documentation for the various connect methods listed above.
 * 
 * This method is called if one of the connect methods are invoked, and the connection is successful.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address{
    
}

/**
 * By design, UDP is a connectionless protocol, and connecting is not needed.
 * However, you may optionally choose to connect to a particular host for reasons
 * outlined in the documentation for the various connect methods listed above.
 * 
 * This method is called if one of the connect methods are invoked, and the connection fails.
 * This may happen, for example, if a domain name is given for the host and the domain name is unable to be resolved.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error{
    
}

/**
 * Called when the datagram with the given tag has been sent.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag{
    
}

/**
 * Called if an error occurs while trying to send a datagram.
 * This could be due to a timeout, or something more serious such as the data being too large to fit in a sigle packet.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
    
}

/**
 * Called when the socket has received the requested datagram.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext{
    
}

/**
 * Called when the socket is closed.
 **/
- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error{
    
}

@end
