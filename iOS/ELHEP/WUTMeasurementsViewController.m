//
//  WUTMeasurementsViewController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTMeasurementsViewController.h"
#import "WUTPlotView.h"
#import "WUTPlotController.h"

@interface WUTMeasurementsViewController ()

@end

@implementation WUTMeasurementsViewController

@synthesize delegate =delegate_;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"MMS", nil);
    
    UIBarButtonItem *startButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Finish", nil)
                                                                    style:UIBarButtonItemStylePlain 
                                                                   target:self
                                                                   action:@selector(finishCapturingData)];
    self.navigationItem.leftBarButtonItem = startButton;
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] initWithFrame:self.view.bounds];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = UITextAlignmentCenter;
    label.text = NSLocalizedString(@"...Initializing", nil);
    [self.view addSubview:label];
    
    CGRect frame = self.isIphone ? CGRectMake(0., 10., 320, 200) : CGRectMake(0., 10., 768, 400);
    
    WUTPlotView *plotView = [[WUTPlotView alloc] initWithFrame:frame dataSource:nil];
    plotView.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    [self.view addSubview:plotView];
    
    WUTPlotController *controller = [[WUTPlotController alloc] initWithPlotView:plotView];
    [controller startSendingResults];
}

- (void)unloadView{
    delegate_ = nil;
}

#pragma mark - Private

- (void)finishCapturingData{
    if ([self.delegate respondsToSelector:@selector(measurementsViewControllerDidFinish:)]) {
        [self.delegate measurementsViewControllerDidFinish:self];
    }
}


@end
