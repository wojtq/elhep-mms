//
//  WUTPlotController.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WUTPlotView;

@interface WUTPlotController : NSObject

- (id)initWithPlotView:(WUTPlotView *)plotView;

- (void)startSendingResults;

- (void)stopSendingResults;

@end
