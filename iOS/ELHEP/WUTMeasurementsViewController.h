//
//  WUTMeasurementsViewController.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTViewController.h"

@protocol WUTMeasurementsViewControllerDelegate ;

@interface WUTMeasurementsViewController : WUTViewController

@property (weak, nonatomic) id<WUTMeasurementsViewControllerDelegate>delegate;

@end

@protocol WUTMeasurementsViewControllerDelegate <NSObject>

@optional

- (void)measurementsViewControllerDidFinish:(WUTMeasurementsViewController *)viewController;

@end