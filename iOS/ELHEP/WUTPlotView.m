//
//  WUTPlotView.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTPlotView.h"
#import "WUTResult.h"


@interface WUTPlotView (){
    struct {
        unsigned int dataSourceNumberOfSamples:1;
        unsigned int dataSourceSampleForIndex:1;
    } _dataSourceFlags;
}


@end

@implementation WUTPlotView
@synthesize dataSource =dataSource_;
/* Colors */
@synthesize backgroundColor = backgroundColor_;
@synthesize boundsColor = boundsColor_;
@synthesize textColor = textColor_;
@synthesize lineColor = lineColor_;
/* Plot parameters */
@synthesize minValue = minValue_;
@synthesize maxValue = maxValue_;

- (id)initWithFrame:(CGRect)frame dataSource:(id<WUTPlotViewDataSource>)dataSource
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = dataSource;
        self.backgroundColor = [UIColor whiteColor];
        self.boundsColor = [UIColor blackColor];
        self.textColor = [UIColor blueColor];
        self.lineColor = [UIColor redColor];
        self.minValue = 0.0f;
        self.maxValue = 1.0f;
        self.opaque = YES;
        
    }
    return self;
}

- (void)setDataSource:(id<WUTPlotViewDataSource>)dataSource{
    
    if (dataSource_ != dataSource) {
        _dataSourceFlags.dataSourceSampleForIndex = [dataSource respondsToSelector:@selector(plotView:sampleAtIndex:)];
        _dataSourceFlags.dataSourceNumberOfSamples = [dataSource respondsToSelector:@selector(numberOfSamplesForPlotView:)];
        
        dataSource_ = dataSource;
    }   
}

- (void)didMoveToSuperview{
    [self setNeedsDisplay];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //Draw background      
    CGContextSetFillColorWithColor(ctx, self.backgroundColor.CGColor);
    CGContextFillRect(ctx, rect);

    
    //Draw bounds
    CGContextSetStrokeColorWithColor(ctx, self.boundsColor.CGColor);
    CGContextSetLineWidth(ctx, 3.0f);
    CGContextStrokeRect(ctx, rect);
    
    
    CGContextSaveGState(ctx);
    CGContextTranslateCTM(ctx, 0.0f, CGRectGetMaxY(rect));
    CGContextScaleCTM(ctx, 1.0f, -1.0f);
    
    NSInteger count;
    
    if (_dataSourceFlags.dataSourceNumberOfSamples){
        count = [self.dataSource numberOfSamplesForPlotView:self];
    }
    else {
        CGContextRestoreGState(ctx);
        return;
    }
    
    NSInteger width = (NSInteger)CGRectGetWidth(rect);
    CGFloat height = CGRectGetHeight(rect);
    {
        
        CGContextSetStrokeColorWithColor(ctx, self.lineColor.CGColor);
        
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, 0, 0);
        
        
        
        if (count < width) {
            
            for (int i = 0; i < count; i++) {
                WUTResult *result = [self.dataSource plotView:self sampleAtIndex:count - 1 - i];
                CGPathAddLineToPoint(path, NULL, i, result.value * height);
            }
        }
        else {

            for (int i = 0; i < width; i++) {
                WUTResult *result = [self.dataSource plotView:self sampleAtIndex:count - 1 - i];
                CGPathAddLineToPoint(path, NULL, i, result.value * height);
            }
            
            
        }
        
        CGContextAddPath(ctx, path);
        CGContextSetLineWidth(ctx, 1.0f);
        CGContextStrokePath(ctx);
        CGPathRelease(path);
    }
    CGContextRestoreGState(ctx);
    
}


@end
