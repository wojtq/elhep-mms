//
//  ProbeOut.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "ProbeOut.h"
#import "Probe.h"


@implementation ProbeOut

@dynamic probeType;
@dynamic dataType;
@dynamic defaultDisplayType;
@dynamic dataUnit;
@dynamic probe;

@end
