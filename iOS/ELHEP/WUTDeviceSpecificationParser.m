//
//  WUTDeviceSpecificationParser.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTDeviceSpecificationParser.h"
#import "TKAppDelegate.h"
#import "Device.h"
#import "Probe.h"
#import "ProbeIn.h"
#import "ProbeOut.h"
#import "ProbeActions.h"
#import "ProbeAction.h"

@interface WUTDeviceSpecificationParser ()

@property (strong, nonatomic) NSData *jsonData;

@property (strong, nonatomic) NSString *jsonFilePath;

@end

@implementation WUTDeviceSpecificationParser
@synthesize devicesSpecDict =devicesSpecDict_;
@synthesize jsonData =jsonData_;
@synthesize jsonFilePath =jsonFilePath_;
@synthesize managedObjectContext =managedObjectContext_;


- (id)initWithPath:(NSString *)jsonFilePath{
    self = [super init];
    if (!self) return nil;
    
    self.jsonFilePath = jsonFilePath;
    
    self.jsonData = [NSData dataWithContentsOfFile:self.jsonFilePath];
    
    if (!self.jsonData ) {
        NSLog(@"Can't create NSData object from file at path: %@", self.jsonFilePath);
        return nil;
    }
    
    return self;
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    TKAppDelegate *delegate = (TKAppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    NSPersistentStoreCoordinator *coordinator = [delegate persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] init];
        [managedObjectContext_ setPersistentStoreCoordinator:coordinator];
        [managedObjectContext_ setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        [managedObjectContext_ setUndoManager:nil];
    }
    return managedObjectContext_;
}

- (BOOL)parseJSON{
    
    NSError * __autoreleasing error = nil;
    
    NSDictionary *spec = [NSJSONSerialization JSONObjectWithData:self.jsonData options:0 error:&error];
    
    if (!spec) {
        NSLog(@"Can't create JSON file, error = %@", [error localizedDescription]);
        return NO;
    }
    
    self.devicesSpecDict = spec;
    
    Device *device = [NSEntityDescription insertNewObjectForEntityForName:@"Device"
                                                   inManagedObjectContext:self.managedObjectContext];
    device.deviceID = [devicesSpecDict_ objectForKey:@"deviceID"];
    device.deviceName = [devicesSpecDict_ objectForKey:@"deviceName"];
    device.verID = [devicesSpecDict_ objectForKey:@"verID"];
    device.dateAdded = [NSDate date];
    
    NSArray *arrayProbes = [devicesSpecDict_ objectForKey:@"uniqueProbes"];
    
    for (NSDictionary *probeDict in arrayProbes) {
        Probe *probe = [NSEntityDescription insertNewObjectForEntityForName:@"Probe"
                                                     inManagedObjectContext:self.managedObjectContext];
        probe.probeName = [probeDict objectForKey:@"probeName"];
        probe.probeID = [probeDict objectForKey:@"probeID"];
        
        NSDictionary *probeSpec = [probeDict objectForKey:@"probeSpec"];
        {
            /* ProbeIn */
            NSDictionary *probeInDict = [probeSpec objectForKey:@"probeIn"];
            ProbeIn *probeIn = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeIn" 
                                                             inManagedObjectContext:self.managedObjectContext];
            probeIn.triggerLow = [probeInDict objectForKey:@"triggerLow"];
            probeIn.triggerHigh = [probeInDict objectForKey:@"triggerHigh"];
            probeIn.sampleFreq = [probeInDict objectForKey:@"sampleFreq"];
            probeIn.numberOfSamples = [probeInDict objectForKey:@"numberOfSamples"];
            probeIn.autoTriggerRetry = [probeInDict objectForKey:@"autoTriggerRetry"];
            
            probe.probeIn = probeIn;
            /* ProbeIn */
        }
        
        {
            /* ProbeOut */
            NSDictionary *probeOutDict = [probeSpec objectForKey:@"probeOut"];
            ProbeOut *probeOut = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeOut" 
                                                             inManagedObjectContext:self.managedObjectContext];
            
            NSString *probeType = [probeOutDict objectForKey:@"probeType"];
            if ([probeType isEqualToString:@"V"]) {
                probeOut.probeType = [NSNumber numberWithInt:1];
            }
            else if ([probeType isEqualToString:@"A"]) {
                probeOut.probeType = [NSNumber numberWithInt:1];
            }
            
            NSString *dataType = [probeOutDict objectForKey:@"dataType"];
            
            if([dataType isEqualToString:@"d1"]){
                probeOut.dataType = [NSNumber numberWithInt:1];
            }
            else if([dataType isEqualToString:@"d2"]){
                probeOut.dataType = [NSNumber numberWithInt:2];
            }
            else if([dataType isEqualToString:@"d3"]){
                probeOut.dataType = [NSNumber numberWithInt:3];
            }
                       
            NSString *defaultDisplayType = [probeOutDict objectForKey:@"defaultDisplayType"];
            if ([defaultDisplayType isEqualToString:@"plot"]) {
                probeOut.defaultDisplayType = [NSNumber numberWithInt:1];
            }
            else if ([defaultDisplayType isEqualToString:@"label"]) {
                probeOut.defaultDisplayType = [NSNumber numberWithInt:2];
            }
            else if ([defaultDisplayType isEqualToString:@"progress"]) {
                probeOut.defaultDisplayType = [NSNumber numberWithInt:3];
            }
            
            probeOut.dataUnit = [probeOutDict objectForKey:@"dataUnit"];

            probe.probeOut = probeOut;
            /* ProbeOut */
        }
        
        {
            /* ProbeActions */
            NSArray *probeActionsArray = [probeSpec objectForKey:@"probeActions"];
            
            if ([probeActionsArray count]) {
                ProbeActions *probeActions = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeActions" inManagedObjectContext:self.managedObjectContext];
                
                for (NSDictionary *probeActionDict in probeActionsArray) {
                    ProbeAction *probeAction = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeAction" inManagedObjectContext:self.managedObjectContext];
                    probeAction.actionName = [probeActionDict objectForKey:@"actionName"];
                    probeAction.actionName = [probeActionDict objectForKey:@"actionDesc"];
                    probeAction.actionParameterType = [self dataTypeStringToNumber:[probeActionDict objectForKey:@"actionParameter"]];
                    [probeActions addActionsObject:probeAction];

                }
                probe.probeActions = probeActions;
            }
            /* ProbeActions */
        }
        
        [device addUniqueProbesObject:probe];
        
        
    }
    
    NSLog(@"uniqueProbess: %@", [device.uniqueProbes description]);
    
    if(![self.managedObjectContext save:&error]){
        return NO;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
    return YES;
    
}

- (NSNumber *)dataTypeStringToNumber:(NSString *)dataType{
    if([dataType isEqualToString:@"d1"]){
        return [NSNumber numberWithInt:1];
    }
    else if([dataType isEqualToString:@"d2"]){
        return [NSNumber numberWithInt:2];
    }
    else if([dataType isEqualToString:@"d3"]){
        return [NSNumber numberWithInt:3];
    }
    else {
        return [NSNumber numberWithInt:0];
    }
}
@end
