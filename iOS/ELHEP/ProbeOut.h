//
//  ProbeOut.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe;

@interface ProbeOut : NSManagedObject

@property (nonatomic, retain) NSNumber * probeType;
@property (nonatomic, retain) NSNumber * dataType;
@property (nonatomic, retain) NSNumber * defaultDisplayType;
@property (nonatomic, retain) NSString * dataUnit;
@property (nonatomic, retain) Probe *probe;

@end
