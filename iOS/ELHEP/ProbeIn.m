//
//  ProbeIn.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "ProbeIn.h"
#import "Probe.h"


@implementation ProbeIn

@dynamic triggerLow;
@dynamic triggerHigh;
@dynamic sampleFreq;
@dynamic autoTriggerRetry;
@dynamic numberOfSamples;
@dynamic probe;

@end
