//
//  ProbeAction.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProbeActions;

@interface ProbeAction : NSManagedObject

@property (nonatomic, retain) NSString * actionName;
@property (nonatomic, retain) NSString * actionDesc;
@property (nonatomic, retain) NSNumber * actionParameterType;
@property (nonatomic, retain) ProbeActions *probeActions;

@end
