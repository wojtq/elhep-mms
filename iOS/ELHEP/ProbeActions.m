//
//  ProbeActions.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "ProbeActions.h"
#import "Probe.h"
#import "ProbeAction.h"


@implementation ProbeActions

@dynamic actions;
@dynamic probe;

- (NSString *)description{
    return @"";//[NSString stringWithFormat:@"probeActions:%@", [self.actions description]];
}

@end
