//
//  WUTPlotView.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WUTResult;

@protocol WUTPlotViewDataSource;

@interface WUTPlotView : UIView

@property (weak, nonatomic) id<WUTPlotViewDataSource> dataSource;

@property (strong, nonatomic) UIColor *backgroundColor;

@property (strong, nonatomic) UIColor *boundsColor;

@property (strong, nonatomic) UIColor *textColor;

@property (strong, nonatomic) UIColor *lineColor;

@property (nonatomic) CGFloat minValue;

@property (nonatomic) CGFloat maxValue;

- (id)initWithFrame:(CGRect)frame dataSource:(id<WUTPlotViewDataSource>)dataSource;

@end

@protocol WUTPlotViewDataSource <NSObject>

- (long long)numberOfSamplesForPlotView:(WUTPlotView *)plotView;

- (WUTResult *)plotView:(WUTPlotView *)plotView sampleAtIndex:(long long)index;

@end
