//
//  WUTViewController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTViewController.h"

@interface WUTViewController ()



@end

@implementation WUTViewController
@synthesize isIphone =isIphone_;
- (id)init{
    self = [super initWithNibName:nil bundle:nil];
    if (!self) {
        return nil;
    }
    return self;
}

- (void)unloadView{

}

- (void)loadView{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0., 0., 320, 480)];
        self.isIphone = YES;
        
    } else {
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0., 0., 768, 1024)];
        self.isIphone = NO;
    }
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    self.view.autoresizesSubviews = YES;
}

- (void)viewDidUnload{
    [super viewDidUnload];
    [self unloadView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
