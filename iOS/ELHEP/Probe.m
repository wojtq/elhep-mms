//
//  Probe.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "Probe.h"
#import "Device.h"
#import "ProbeActions.h"
#import "ProbeIn.h"
#import "ProbeOut.h"


@implementation Probe

@dynamic probeID;
@dynamic probeName;
@dynamic device;
@dynamic probeIn;
@dynamic probeOut;
@dynamic probeActions;

- (NSString *)description{
    return [NSString stringWithFormat:@"probeId: %@,\nprobeName: %@,\nprobeIn:%@,\nprobeOut:%@,\nprobeActions: %@",
            [self.probeID description],
            self.probeName,
            [self.probeIn description],
            [self.probeOut description],
            [self.probeActions description]];
}

@end
