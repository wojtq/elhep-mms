//
//  WUTSSDPHandler.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 6/19/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCDAsyncUdpSocket;

@interface WUTSSDPHandler : NSObject{
    GCDAsyncUdpSocket *udpSocket_;
}

- (void)startSendingAdvertisements;

@end
