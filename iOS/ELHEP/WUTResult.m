//
//  WUTResult.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTResult.h"

@implementation WUTResult

@synthesize timeStamp =timeStamp_;

@synthesize value =value_;

- (id)initWithValue:(CGFloat)value andTimeStamp:(NSDate *)timeStamp{
    self = [super init];
    if(!self)return nil;
    self.value = value;
    self.timeStamp = timeStamp;
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"WUTResult<%f,%@>", value_, timeStamp_];
}

@end
