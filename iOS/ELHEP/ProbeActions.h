//
//  ProbeActions.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe, ProbeAction;

@interface ProbeActions : NSManagedObject

@property (nonatomic, retain) NSSet *actions;
@property (nonatomic, retain) Probe *probe;
@end

@interface ProbeActions (CoreDataGeneratedAccessors)

- (void)addActionsObject:(ProbeAction *)value;
- (void)removeActionsObject:(ProbeAction *)value;
- (void)addActions:(NSSet *)values;
- (void)removeActions:(NSSet *)values;

@end
