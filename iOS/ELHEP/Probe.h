//
//  Probe.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device, ProbeActions, ProbeIn, ProbeOut;

@interface Probe : NSManagedObject

@property (nonatomic, retain) NSNumber * probeID;
@property (nonatomic, retain) NSString * probeName;
@property (nonatomic, retain) Device *device;
@property (nonatomic, retain) ProbeIn *probeIn;
@property (nonatomic, retain) ProbeOut *probeOut;
@property (nonatomic, retain) ProbeActions *probeActions;

@end
