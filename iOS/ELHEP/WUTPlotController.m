//
//  WUTPlotController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTPlotController.h"
#import "WUTPlotView.h"
#import "WUTResult.h"

#define RANDOM_SEED() srandom((unsigned)(mach_absolute_time() & 0xFFFFFFFF))
#define RANDOM_NUM() (random())


@interface WUTPlotController () <WUTPlotViewDataSource>{
    double time;
    double period;
    float samplingFreq;
    
    NSMutableArray *resultsArray_;
}

@property (strong, nonatomic) NSTimer *timer;

@property (strong, nonatomic) WUTPlotView *plotView;

@end

@implementation WUTPlotController

@synthesize plotView =plotView_;

@synthesize timer =timer_;

- (id)initWithPlotView:(WUTPlotView *)plotView{
    self = [self init];
    if(!self) return nil;
    
    self.plotView = plotView;
    
    plotView.dataSource = self;
    
    return self;
}

- (id)init{
    self = [super init];
    if(!self) return nil;
    
    time = 0;
    period = 10;
    samplingFreq = 0.01;
    resultsArray_ = [[NSMutableArray alloc]initWithCapacity:0];
    
    return self;
}

- (void)startSendingResults{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:samplingFreq
                                     target:self 
                                   selector:@selector(sendResult)
                                   userInfo:nil 
                                    repeats:YES];
}

- (void)stopSendingResults{
    
    [self.timer invalidate];
    self.timer = nil;
}

- (CGFloat)sinValueForTime:(double)aTime{
    return (sin(aTime/period) + 1) * 0.5;
}

- (CGFloat)sinexValueForTime:(double)aTime{
    CGFloat exp = expf(-aTime);
    
    if (exp < 0.1) {
        exp = expf(aTime);
    }
    
    if(exp > 1 ){
        exp = expf(-aTime);
    }
    
    return exp * (sin(aTime/period) + 1) * 0.5;
}

- (void)sendResult{
    WUTResult *result = [[WUTResult alloc] initWithValue:[self sinValueForTime:time] andTimeStamp:nil];
    [resultsArray_ addObject:result];
    time ++;
    
    [self.plotView setNeedsDisplay];
}

#pragma mark - WUTPlotViewDataSource

- (long long)numberOfSamplesForPlotView:(WUTPlotView *)plotView{
    return (long long)[resultsArray_ count];
}

- (WUTResult *)plotView:(WUTPlotView *)plotView sampleAtIndex:(long long)index{
    return [resultsArray_ objectAtIndex:(NSInteger)index];
}

@end
