//
//  ProbeIn.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe;

@interface ProbeIn : NSManagedObject

@property (nonatomic, retain) NSNumber * triggerLow;
@property (nonatomic, retain) NSNumber * triggerHigh;
@property (nonatomic, retain) NSNumber * sampleFreq;
@property (nonatomic, retain) NSNumber * autoTriggerRetry;
@property (nonatomic, retain) NSNumber * numberOfSamples;
@property (nonatomic, retain) Probe *probe;

@end
