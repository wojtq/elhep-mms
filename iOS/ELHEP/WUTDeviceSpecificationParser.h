//
//  WUTDeviceSpecificationParser.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WUTDeviceSpecificationParser : NSObject

@property (strong, nonatomic) NSDictionary *devicesSpecDict;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (id)initWithPath:(NSString *)jsonFilePath;

- (BOOL)parseJSON;

@end
