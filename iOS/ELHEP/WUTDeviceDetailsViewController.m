//
//  WUTDeviceDetailsViewControllerViewController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTDeviceDetailsViewController.h"
#import "WUTMeasurementsViewController.h"
#import "Device.h"
#import "Probe.h"

@interface WUTDeviceDetailsViewController ()

@property (strong,  nonatomic) Device *device;

@end

@implementation WUTDeviceDetailsViewController

@synthesize tableView =tableView_;
@synthesize device = device_;


- (id)initWithDevice:(Device *)device{
    self = [super initWithNibName:nil bundle:nil];
    if (!self) return nil;
    
    self.device = device;
    
    return self;
}


- (void)unloadView{
    device_ = nil;
    tableView_ = nil;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.title = [self.device deviceName];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    
    UIBarButtonItem *startButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Start", nil)
                                                                    style:UIBarButtonItemStylePlain 
                                                                   target:self
                                                                   action:@selector(startCapturingData)];
    
    self.navigationItem.rightBarButtonItem = startButton;
    
}
#pragma mark - Private

- (void)startCapturingData{
    WUTMeasurementsViewController *vc = [[WUTMeasurementsViewController alloc] init];
    vc.delegate = (id<WUTMeasurementsViewControllerDelegate>)self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.navigationController presentModalViewController:navController animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.device uniqueProbes] count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"probeName" ascending:YES];
    
    NSError * __autoreleasing error = nil;
    NSArray *array = [self.device.uniqueProbes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    if (!array) {
        NSLog(@"error fetching: %@", [error localizedDescription]);
    }
    
    Probe *probe = [array objectAtIndex:indexPath.row];
    
    cell.textLabel.text = probe.probeName;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"probeName" ascending:YES];

    NSError * __autoreleasing error = nil;
    NSArray *array = [self.device.uniqueProbes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    if (!array) {
        NSLog(@"error fetching: %@", [error localizedDescription]);
    }
    
    Probe *probe = [array objectAtIndex:section];
    
    return [NSString stringWithFormat:@"%@", probe.probeName];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

#pragma mark - WUTMeasurementsViewControllerDelegate

- (void)measurementsViewControllerDidFinish:(WUTMeasurementsViewController *)viewController{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

@end
