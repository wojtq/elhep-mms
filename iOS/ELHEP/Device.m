//
//  Device.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "Device.h"
#import "Probe.h"


@implementation Device

@dynamic deviceID;
@dynamic verID;
@dynamic deviceName;
@dynamic dateAdded;
@dynamic uniqueProbes;

- (NSString *)description{
    
    NSMutableString *uniqueProbes = [[NSMutableString alloc] initWithCapacity:0];
    
    for (Probe *probe in self.uniqueProbes) {
        [uniqueProbes appendFormat:@"%@", [probe description]];
    }
    
    return [NSString stringWithFormat:@"deviceID:%@,\ndeviceName: %@,\nverID: %@,\ndateAdded: %@,\nuniqueProbes :%@",
            [self.deviceID description], self.deviceName, [self.verID description],[self.dateAdded description],uniqueProbes];
}

@end
