//
//  ProbeAction.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/31/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "ProbeAction.h"
#import "ProbeActions.h"


@implementation ProbeAction

@dynamic actionName;
@dynamic actionDesc;
@dynamic actionParameterType;
@dynamic probeActions;

- (NSString *)description{
    return @"";//[NSString stringWithFormat:@"actionName: %@, actionDesc: %@, actionParameterType: %@",
            //self.actionName, self.actionDesc, [self.actionParameterType description]];
}

@end
